# Scope
During my work, I sometimes find myself performing certain configurations or operations which I may have to be able to repeat. I thus created this wiki with the intention of using it to log and document such configurations/operations. 

# Git
[Jupyter notebooks with Git](https://gitlab.com/slcu/teamHJ/niklas/wiki/wikis/jupyter-notebooks-with-git)